-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.17-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para todo_db
CREATE DATABASE IF NOT EXISTS `todo_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `todo_db`;

-- Volcando estructura para tabla todo_db.accesorios
CREATE TABLE IF NOT EXISTS `accesorios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FK_inventario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_inventario_id` (`FK_inventario`),
  CONSTRAINT `FK_inventario_id` FOREIGN KEY (`FK_inventario`) REFERENCES `inventario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.accesorios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `accesorios` DISABLE KEYS */;
/*!40000 ALTER TABLE `accesorios` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.administrador
CREATE TABLE IF NOT EXISTS `administrador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombreAdministrador` char(60) NOT NULL,
  `numeroTelefono` char(12) NOT NULL,
  `correoElectronico` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.administrador: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `administrador` DISABLE KEYS */;
/*!40000 ALTER TABLE `administrador` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.categoriadeaccesorios
CREATE TABLE IF NOT EXISTS `categoriadeaccesorios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FK_Accesorios` int(11) NOT NULL,
  `FK_Inventario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_accesorios_id` (`FK_Accesorios`),
  KEY `FK_inventario_id2` (`FK_Inventario`),
  CONSTRAINT `FK_accesorios_id` FOREIGN KEY (`FK_Accesorios`) REFERENCES `accesorios` (`id`),
  CONSTRAINT `FK_inventario_id2` FOREIGN KEY (`FK_Inventario`) REFERENCES `inventario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.categoriadeaccesorios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categoriadeaccesorios` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoriadeaccesorios` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.citas
CREATE TABLE IF NOT EXISTS `citas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FK_usuario` int(11) NOT NULL,
  `FK_talleres` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_usuario_id` (`FK_usuario`),
  KEY `FK_talleres_id` (`FK_talleres`),
  CONSTRAINT `FK_talleres_id` FOREIGN KEY (`FK_talleres`) REFERENCES `talleres` (`id`),
  CONSTRAINT `FK_usuario_id` FOREIGN KEY (`FK_usuario`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.citas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `citas` DISABLE KEYS */;
/*!40000 ALTER TABLE `citas` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.detallesdeventas
CREATE TABLE IF NOT EXISTS `detallesdeventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FK_ventas` int(11) NOT NULL,
  `FK_accesorios` int(11) NOT NULL,
  `cantidadDeAccesorios` int(11) DEFAULT NULL,
  `FK_citas` int(11) NOT NULL,
  `FK_seguros` int(11) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ventas_id` (`FK_ventas`),
  KEY `FK_accesorios_id2` (`FK_accesorios`),
  KEY `FK_citas_id` (`FK_citas`),
  KEY `FK_seguros_id` (`FK_seguros`),
  CONSTRAINT `FK_accesorios_id2` FOREIGN KEY (`FK_accesorios`) REFERENCES `accesorios` (`id`),
  CONSTRAINT `FK_citas_id` FOREIGN KEY (`FK_citas`) REFERENCES `citas` (`id`),
  CONSTRAINT `FK_seguros_id` FOREIGN KEY (`FK_seguros`) REFERENCES `seguros` (`id`),
  CONSTRAINT `FK_ventas_id` FOREIGN KEY (`FK_ventas`) REFERENCES `ventas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.detallesdeventas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `detallesdeventas` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallesdeventas` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.galeriadeimagenes
CREATE TABLE IF NOT EXISTS `galeriadeimagenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FK_talleres` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_talleres_id2` (`FK_talleres`),
  CONSTRAINT `FK_talleres_id2` FOREIGN KEY (`FK_talleres`) REFERENCES `talleres` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.galeriadeimagenes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `galeriadeimagenes` DISABLE KEYS */;
/*!40000 ALTER TABLE `galeriadeimagenes` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.inventario
CREATE TABLE IF NOT EXISTS `inventario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` int(11) NOT NULL,
  `FK_administrador` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_administrador_id` (`FK_administrador`),
  CONSTRAINT `FK_administrador_id` FOREIGN KEY (`FK_administrador`) REFERENCES `administrador` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.inventario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `inventario` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventario` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.seguros
CREATE TABLE IF NOT EXISTS `seguros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.seguros: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `seguros` DISABLE KEYS */;
/*!40000 ALTER TABLE `seguros` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.talleres
CREATE TABLE IF NOT EXISTS `talleres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` char(60) NOT NULL,
  `nombreTalleres` char(60) NOT NULL,
  `numeroTelefono` char(12) NOT NULL,
  `correoElectronico` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.talleres: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `talleres` DISABLE KEYS */;
/*!40000 ALTER TABLE `talleres` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.task
CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.task: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT IGNORE INTO `task` (`id`, `description`, `completed`) VALUES
	(1, 'Taskñó 1', 1),
	(2, 'Task 2', 0),
	(3, 'Task 3', 0),
	(4, 'Task 4', 0),
	(17, 'hello', 0);
/*!40000 ALTER TABLE `task` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombreUsuario` char(60) NOT NULL,
  `numeroTelefono` char(12) NOT NULL,
  `correoElectronico` char(50) NOT NULL,
  `direccion` char(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

-- Volcando estructura para tabla todo_db.ventas
CREATE TABLE IF NOT EXISTS `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `FK_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_usuario_id2` (`FK_usuario`),
  CONSTRAINT `FK_usuario_id2` FOREIGN KEY (`FK_usuario`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla todo_db.ventas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
